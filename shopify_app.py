import requests
import json
import dataset
from flask import Flask
import threading

from time import time, sleep


get_url = 'https://a38f4a6a8cb713fe2bebdbf3df331f54:3182dcd29ff6c3f6f2dd325ba99b4216@mishipaytestdevelopmentemptystore.myshopify.com/admin/api/2021-01/'


def get_product():
    end_point = 'products.json'
    prod_req = requests.get(get_url+end_point)
    return prod_req.json()['products']

def parse_products(prod_data):
    products=[]
    for item in prod_data:
        mainid = item['id']
        title = item['title']
        published_at = item['published_at']
        product_type = item['product_type']
        for v in item['variants']:
            k = {
                'id' : mainid,
                'title' : title,
                'published_at' : published_at,
                'product_type' : product_type,
                'varid' : v['id'],
                'vartitle' :v['title'],
                'sku' :v['sku'],
                'price' :v['price'],
                'created_at' :v['created_at'],
                'updated_at' :v['updated_at'],
                'compare_at_price' : v['compare_at_price']
            }
            products.append(k)
    return products

def update_table():
    while True:
        db.query('DELETE FROM product_table')
        prod_data  = get_product()
        product_list = parse_products(prod_data)
        for i in product_list:
            if not table.find_one(id=i['id']):
                table.insert(i)
        sleep(60 - time() % 60)

db = dataset.connect('sqlite:///products.db')
table = db.create_table('product_table',primary_id='id')

t1 = threading.Thread(target=update_table, args=())
t1.start()

api = Flask(__name__)
@api.route('/product', methods=['GET'])
def get_products():
    result = db.query('SELECT id, title, price FROM product_table')
    l=[]
    for i in result:
        l.append(i)
    return json.dumps(l)

if __name__ == '__main__':
    api.run()

